# nextcloud-php-modules

> ‼️ This project was migrated to https://github.com/docker-alw/nextcloud-php-modules.git. ‼️
>
> The new repository supports ARM based images and will be maintained.

---

Docker image based on alpine with all modules required for Nextcloud installed.

It is intended to be used as base image for https://gitlab.com/docker-alw/nextcloud and https://gitlab.com/docker-alw/nextcloud-cron.

The following packages are installed:

* ffmpeg
* imagemagick
* libreoffice
* php82-bcmath
* php82-bz2
* php82-ctype
* php82-curl
* php82-dom
* php82-exif
* php82-fileinfo
* php82-gd
* php82-gettext
* php82-gmp
* php82-iconv
* php82-imap
* php82-intl
* php82-mbstring
* php82-opcache
* php82-openssl
* php82-pcntl
* php82-pdo_mysql
* php82-pecl-apcu
* php82-pecl-imagick
* php82-posix
* php82-session
* php82-simplexml
* php82-sodium
* php82-sysvsem
* php82-xml
* php82-xmlreader
* php82-xmlwriter
* php82-zip
